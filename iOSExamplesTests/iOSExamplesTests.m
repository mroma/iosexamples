//
//  iOSExamplesTests.m
//  iOSExamplesTests
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import "iOSExamplesTests.h"

@implementation iOSExamplesTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in iOSExamplesTests");
}

@end
