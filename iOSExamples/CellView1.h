//
//  cellView1.h
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellView1 : UITableViewCell
{
    UILabel *titleLabel;
    UILabel *textLabel;
}

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *textLabel;

@end
