//
//  ButtonDemoController.h
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ButtonDemoController : UIViewController
{
    // define buttons
    UIButton *button1;
    UIButton *button2;
    
    // text fields
    UITextField *text1;
}

@property (nonatomic, strong) IBOutlet UIButton *button1;
@property (nonatomic, strong) IBOutlet UIButton *button2;
@property (nonatomic, strong) IBOutlet UITextField *text1;

@end
