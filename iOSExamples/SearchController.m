//
//  SearchController.m
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import "SearchController.h"

@interface SearchController ()

@end

@implementation SearchController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // load the initial list of items to search
    allItems = [[NSArray alloc] initWithObjects:@"One", @"Two", @"Three", @"Four", @"Five", @"Six", @"Seven", @"Eight", @"Nine", @"Ten", @"Eleven", @"Twelve", nil];
    
    // set initial display to all items
    displayItems = [[NSMutableArray alloc] initWithArray:allItems];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// search action
- (void) searchBar: (UISearchBar *) searchBar textDidChange:(NSString *)searchText
{NSLog(@"here");
    // check if no text
    if( searchText.length == 0)
    {
        // reset display items
        [displayItems removeAllObjects];
        [displayItems addObjectsFromArray:allItems];
    }
    // else, return set of items that match
    else
    {
        // reset display items
        [displayItems removeAllObjects];
        
        // go through each item
        for(NSString *s in allItems)
        {
            // check if search text contained
            if( [s rangeOfString:searchText options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                // if so, add the item to the list of items to display
                [displayItems addObject:s];
            }
        }
    }
    
    // reload the data
    [tableView reloadData];
}

// search button action, hide keyboard
- (void) searchBarSearchButtonClicked:(UISearchBar *)aSearchBar
{
    [searchBar resignFirstResponder];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // only 1 section
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // return the number of data records to show
    return displayItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)atableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get the cell to display
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // show the current menu item
    cell.textLabel.text = [displayItems objectAtIndex:indexPath.row];;
    
    return cell;
}

// 
@end
