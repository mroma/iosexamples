//
//  mainController.h
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import <UIKit/UIKit.h>

// interface
@interface MainController : UITableViewController
{
    // define list of menu items
    NSArray     *menuItems;
}

// properties
@property (nonatomic, strong) NSArray *menuItems;

@end
