//
//  twitterController.m
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import "TwitterController.h"

@interface TwitterController ()

@end

@implementation TwitterController
@synthesize tableView;
@synthesize searchTxt;
@synthesize data;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // set default search, load initial data
    searchTxt.text = @"dinosaur";
    [self loadTwitterData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // only 1 section
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // return the number of data records to show
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get the cell to display
    static NSString *CellIdentifier = @"Cell";
    CellView1 *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CellView1 alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // show the current menu item
    NSDictionary *tweet = (NSDictionary *)[data objectAtIndex:[indexPath row]];
    cell.titleLabel.text = [tweet valueForKey:@"from_user_name"];
    cell.textLabel.text = [tweet valueForKey:@"text"];
    
    return cell;
}

// search button click
- (IBAction)searchButtonClick:(id)sender {
    NSLog(@"Search button pressed");
    
    // load twitter data
    [self loadTwitterData];
}

/***
 Twitter methods
 ***/

// function that makes a request for twitter json
-(void) loadTwitterData
{
    // Get device unique ID
    //UIDevice *device = [UIDevice currentDevice];
    NSString* urlStr = @"http://search.twitter.com/search.json?callback=?&q=";
    urlStr = [urlStr stringByAppendingString:searchTxt.text];
    
    NSLog(urlStr);
    
    // Start request
    NSURL *url = [NSURL URLWithString:urlStr];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    /*[request setPostValue:@"6600" forKey:@"postalcode"];
    [request setPostValue:@"AT" forKey:@"country"];
    [request setPostValue:@"demo" forKey:@"username"];*/
    [request setDelegate:self];
    [request startAsynchronous];
}


// aync finish
- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"async completed");
    
    NSString *msg = @"none";
    
    if (request.responseStatusCode == 400) {
        msg = @"error 400";
    } else if (request.responseStatusCode == 403) {
        msg = @"error 403";
    } else if (request.responseStatusCode == 200) {
        
        // get the request
        NSString *responseString = [request responseString];
        NSDictionary *responseDict = [responseString JSONValue];
        
        // parse json to dictionary
        data = (NSArray *)[responseDict valueForKey:@"results"];
        
        // reload table
        [tableView reloadData];
        
    } else {
        msg = @"Unexpected error";
    }
    
    NSLog(@"log: %i ", request.responseStatusCode);
    NSLog(msg);
}

@end
