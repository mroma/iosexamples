//
//  ButtonDemoController.m
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import "ButtonDemoController.h"

@interface ButtonDemoController ()

@end

@implementation ButtonDemoController
@synthesize text1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// button actions

// button 1
- (IBAction)button1Click:(id)sender
{
    NSLog(@"button 1 pressed");
}

// button 2
- (IBAction)button2Click:(id)sender
{
    NSLog(@"button 2 pressed, text: %@", text1.text);
}


@end
