//
//  SearchController.h
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchController : UIViewController<UISearchBarDelegate>
{
    NSArray         * allItems;
    NSMutableArray  * displayItems;
    
    IBOutlet UITableView     * tableView;
    IBOutlet UISearchBar     * searchBar;
}

@end
