//
//  mainController.m
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import "MainController.h"
#import "TwitterController.h"
#import "ButtonDemoController.h"
#import "SearchController.h"
#import "AlertDemoController.h";

@interface MainController ()
@end

@implementation MainController
@synthesize menuItems;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // load menu items
    self.menuItems = [[NSArray alloc] initWithObjects:
                       @"Twitter feed",
                       @"Buttons",
                       @"Search",
                       @"Alerts",
                       nil];
    
    // set title
    self.title = @"iOS Examples";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // get the cell to display
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // show the current menu item
    cell.textLabel.text = [menuItems objectAtIndex:[indexPath row]];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // check selection
    switch ([indexPath row]) {
            
        // twitter
        case 0:
            NSLog(@"twitter");
            [self loadTwitter];
            break;
            
        // twitter
        case 1:
            NSLog(@"button demo");
            [self loadButtonDemo];
            break;
            
        // search
        case 2:
            NSLog(@"search demo");
            [self loadSearchDemo];
            break;
            
            // alert
        case 3:
            NSLog(@"alert demo");
            [self loadAlertDemo];
            break;
    }
    NSLog(@"click");
}

// function that loads twitter
- (void)loadTwitter
{
    // init controller
    TwitterController *controller = [[TwitterController alloc] initWithNibName:@"TwitterController" bundle:nil];

    // set title and load
    controller.title = @"Twitter feed";
    [self.navigationController pushViewController:controller animated:YES];
}

// function that loads button demo
- (void)loadButtonDemo
{
        // init controller
    ButtonDemoController *controller = [[ButtonDemoController alloc] initWithNibName:@"ButtonDemoController" bundle:nil];
    
    // set title and load
    controller.title = @"Button Demo";
    [self.navigationController pushViewController:controller animated:YES];
}

// function that loads the search demo
- (void)loadSearchDemo
{
    // init controller
    SearchController *c = [[SearchController alloc] initWithNibName:@"SearchController" bundle:nil];
    
    // set title and load
    c.title = @"Search Demo";
    [self.navigationController pushViewController:c  animated:YES];
    
}

// function that loads the alert demo
- (void)loadAlertDemo
{
    // init controller
    AlertDemoController *c = [[AlertDemoController alloc] initWithNibName:@"AlertDemoController" bundle:nil];
    
    // set title and load
    c.title = @"Alert Demo";
    [self.navigationController pushViewController:c  animated:YES];
}

@end
