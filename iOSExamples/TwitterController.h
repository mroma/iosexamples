//
//  twitterController.h
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "SBJSON.h"
#import "CellView1.h"

@interface TwitterController : UIViewController
{
    // define controls
    UITableView *tableView;
    UITextField *searchTxt;
    
    // define data
    NSArray *data;
}

// properties
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UITextField *searchTxt;
@property (nonatomic, strong) NSArray *data;

@end
