//
//  mroma_iosdemoAppDelegate.h
//  iOSExamples
//
//  Created by Michael Roma on 11/3/12.
//  Copyright (c) 2012 Michael Roma. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainController;

@interface mroma_iosdemoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
